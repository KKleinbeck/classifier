CC=g++
PROJECTPATHS=-I./source -I./source/csvhandler -I./source/nnfind -I./source/iohandler
EXTERNALPATHS=-I/usr/include/boost_1_62_0/
CFLAGS=-std=c++17 $(PROJECTPATHS) $(EXTERNALPATHS) -O1 -Wall -c
BOOSTOPTIONS=-lboost_program_options


MAINDIR = ./source/
MAIN = $(patsubst $(MAINDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(MAINDIR)*.cpp))
CSVDIR = $(MAINDIR)csvhandler/
CSV = $(patsubst $(CSVDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(CSVDIR)*.cpp))
NNFDIR = $(MAINDIR)nnfind/
NNF = $(patsubst $(NNFDIR)%.cpp,$(BUILDDIR)%.o,$(wildcard $(NNFDIR)*.cpp))
BUILDDIR = ./obj/

all: dir linking

linking: main csvhandler nnf
	$(CC) -std=c++11 -Wall $(MAIN) $(CSV) $(NNF) -o classifier $(BOOSTOPTIONS)

dir:
	mkdir -p $(BUILDDIR)

main: $(MAIN) nnf

$(MAIN): $(BUILDDIR)%.o: $(MAINDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@

csvhandler: $(CSV)

$(CSV): $(BUILDDIR)%.o: $(CSVDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@

nnf: $(NNF)

$(NNF): $(BUILDDIR)%.o: $(NNFDIR)%.cpp
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)
