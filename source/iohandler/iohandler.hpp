#pragma once
//------------------------------------------------
//  IOHANDLER.HPP
//------------------------------------------------

#ifndef IOHANDLER_HPP
#define IOHANDLER_HPP

#include <algorithm>
#include <string>
#include <cstdlib>

// My Libraries


namespace io
{

// Typedefs and Structures

struct input_params
{
	std::string filename;
	size_t k_max, l;

	input_params() : filename(""), k_max(0), l(0) {};
};

// free functions

char *getCmdOption(char **begin, char **end, const std::string &option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return nullptr;
}

void parse_options(int argc, char *argv[], input_params &input)
{
	char *res(getCmdOption(argv, argv + argc, "-n"));
	if(res != nullptr) input.filename.assign(res);
	res = getCmdOption(argv, argv + argc, "-k");
	if(res != nullptr) input.k_max = strtoul(res, nullptr, 10);
	res = getCmdOption(argv, argv + argc, "-l");
	if(res != nullptr) input.l = strtoul(res, nullptr, 10);

	assert(input.k_max != 0);
	assert(input.l     != 0);
}

} // namespace io
#endif



