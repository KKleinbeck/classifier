#pragma once
//------------------------------------------------
//  POINTS.HPP
//------------------------------------------------

#ifndef POINTS_HPP
#define POINTS_HPP

#include <vector>

// My Libraries

// Typedefs and Structures

template<typename T>
using point = std::vector<T>;

template<typename T>
struct neighbourhood
{
	point<T> target;
	T target_class;
	std::vector<T> distances;
	std::vector<point<T>> neighbours;
	std::vector<T> neighbours_class;

	neighbourhood(point<T> target_in, size_t num_neighbours, T class_in = 0) :
		target(target_in), target_class(class_in), distances(std::vector<T>(num_neighbours, 1e99)),
		neighbours(std::vector<point<T>>(num_neighbours)), neighbours_class(num_neighbours) {};

	bool insert_neighbour_candidate(point<T> candidate, T candidate_class);
};

// free functions

unsigned uipow(unsigned base, unsigned exp);

template<typename T>
T dist(point<T> a, point<T> b);

template<typename T>
size_t find_larger(const T val, const std::vector<T>& list);

template<typename T>
void determine_misclassifications(const neighbourhood<T>& current_neighbourhood,
		std::vector<unsigned>& misclassifications);

template<typename T>
size_t index_to_smallest(std::vector<T> &data);

#endif
