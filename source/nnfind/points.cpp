//------------------------------------------------
// POINTS.CPP
//------------------------------------------------

#include "points.hpp"

// member functions

template<typename T>
bool neighbourhood<T>::insert_neighbour_candidate(point<T> candidate, T candidate_class)
{
	T candidate_distance(dist(target, candidate));
	size_t distance_index(find_larger(candidate_distance, distances));
	if(distance_index != neighbours.size() )
	{
		distances.insert(distances.begin() + distance_index, candidate_distance);
		distances.pop_back();
		neighbours.insert(neighbours.begin() + distance_index, candidate);
		neighbours.pop_back();
		neighbours_class.insert(neighbours_class.begin() + distance_index, candidate_class);
		neighbours_class.pop_back();

		return true;
	}
	return false;
}

// free functions

unsigned uipow(unsigned base, unsigned exp)
{
	unsigned res(1);
	for(; exp != 0; exp >>= 1)
	{
		if(exp & 1) res *= base;
		base *= base;
	}
	return res;
}

template<typename T>
T dist(point<T> a, point<T> b)
{
	assert(a.size()==b.size());
	
	T dist = 0;
	for(size_t i = 0; i < a.size(); ++i)
	{
		dist += (a[i]-b[i])*(a[i]-b[i]);
	}
	return dist;
}
	
template<typename T>
size_t find_larger(const T val, const std::vector<T>& list)
{
	size_t iter = 0;
	for(; iter < list.size() && val > list[iter]; ++iter) ;

	return iter;
}

template<typename T>
void determine_misclassifications(const neighbourhood<T>& current_neighbourhood,
		std::vector<unsigned>& misclassifications)
{
	assert(misclassifications.size() == current_neighbourhood.neighbours.size() );

	T accumulated_classes(0);
	for(size_t k = 0; k < misclassifications.size(); ++k)
	{
		accumulated_classes += current_neighbourhood.neighbours_class[k];
		misclassifications[k] =
			(current_neighbourhood.target_class * accumulated_classes) >= 0;
	}
}

template<typename T>
size_t index_to_smallest(std::vector<T> &data)
{
	size_t index(0);
	T smallest(data[0]);
	for(size_t i = 0; i < data.size(); ++i)
	{
		if(data[i] < smallest)
		{
			smallest = data[i];
			index = i;
		}
	}
	return index;
}

template bool neighbourhood<double>::insert_neighbour_candidate(point<double> candidate,
		double candidate_class);
template size_t find_larger(const double val, const std::vector<double>& list);
template void determine_misclassifications(const neighbourhood<double>& current_neighbourhood,
		std::vector<unsigned>& misclassifications);
template size_t index_to_smallest(std::vector<double> &data);
