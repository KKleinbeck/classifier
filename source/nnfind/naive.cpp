//------------------------------------------------
// NAIVE.CPP
//------------------------------------------------

#include "naive.hpp"

#include <algorithm>

// free functions

template<typename T>
void populate_neighbourhoods(std::vector<neighbourhood<T>>& neighbourhoods, csvhandler& input)
{
	std::vector<double> line;
	while(input.get_line(line))
	{
		for(auto& current_neighbourhood : neighbourhoods)
		{
			current_neighbourhood.insert_neighbour_candidate(
				point<double>(line.begin()+1, line.end() ), line[0] );
		}
	}
}

template void populate_neighbourhoods<double>(std::vector<neighbourhood<double>>& cn,
		csvhandler& input);
