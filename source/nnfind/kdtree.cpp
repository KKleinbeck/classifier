//------------------------------------------------
// KDTREE.CPP
//------------------------------------------------

#include "kdtree.hpp"

#include <cmath>

using namespace tree;

// free functions

// member functions

// BRANCH

template<typename T>
branch<T>::branch(size_t in_max_depth, point<T> in_splitting_point,
		branch<T> *in_root) :
	root(in_root), splitting_point(in_splitting_point), max_depth(in_max_depth)
{
	if(root == nullptr)
	{
		depth = 0;
	}
	else
	{
		depth = root->depth + 1;
	}
	
	if(depth == max_depth)
	{
		left_child = new leaf(this);
		right_child = new leaf(this);
	}
	else
	{
		point<T> nsp(splitting_point);
		nsp[depth%nsp.size()] += pow(0.5, 1. + (depth/nsp.size()));
		left_child = new branch(max_depth, nsp, this);
		nsp[depth%nsp.size()] -= pow(0.5, (depth/nsp.size()));
		right_child = new branch(max_depth, nsp, this);
	}
}

template<typename T>
void branch<T>::insert_point(point<T> in_point, T in_class)
{
	if(in_point[depth%in_point.size()] > splitting_point[depth%splitting_point.size()])
	{
		right_child->insert_point(in_point, in_class);
	}
	else
	{
		left_child->insert_point(in_point, in_class);
	}
}

template<typename T>
void branch<T>::locate_nearest_neighbours(neighbourhood<T>& current_neighbourhood)
{
	T distance (dist(current_neighbourhood.target, splitting_point) );
	if(current_neighbourhood.target[depth%splitting_point.size()] >
			splitting_point[depth%splitting_point.size()])
	{
		right_child->locate_nearest_neighbours(current_neighbourhood);
		if(distance > current_neighbourhood.distances.back())
		{
			left_child->locate_nearest_neighbours(current_neighbourhood);
		}
	}
	else
	{
		left_child->locate_nearest_neighbours(current_neighbourhood);
		if(distance > current_neighbourhood.distances.back())
		{
			right_child->locate_nearest_neighbours(current_neighbourhood);
		}
	}
}

// LEAF

template<typename T>
void leaf<T>::insert_point(point<T> in_point, T in_class)
{
	leaves.push_back(in_point);
	classes.push_back(in_class);
}

template<typename T>
void leaf<T>::locate_nearest_neighbours(neighbourhood<T>& current_neighbourhood)
{
	for(size_t i = 0; i < leaves.size(); ++i)
	{
		current_neighbourhood.insert_neighbour_candidate(leaves[i], classes[i]);
	}
}

template branch<double>::branch(size_t id, point<double> isp, branch<double> *ir);
template void branch<double>::insert_point(point<double> in_point, double in_class);
template void branch<double>::locate_nearest_neighbours(neighbourhood<double>& cn);
