#pragma once
//------------------------------------------------
//  KDTREE.HPP
//------------------------------------------------

#ifndef KDTREE_HPP
#define KDTREE_HPP

#include "points.hpp"

// My Libraries


namespace tree
{

// free functions

template<typename T>
inline size_t get_index(point<T> P, size_t depth)
{
	size_t index(0);
	bool current_index_digit;
	for(size_t j, i = 0; i < depth; ++i)
	{
		j = i % P.size();
		current_index_digit = P[j] >= 0;
		P[j] = 2*(P[j] + 0.5 - current_index_digit);
		index = (index << 1) | current_index_digit;
	}

	return index;
}

// Typedefs and Structures

//template<typename T>
//using branch = std::vector<point<T>>;

template<typename T>
class kd_tree
{
	public:
		virtual ~kd_tree() {}

		virtual void insert_point(point<T> in_point, T in_class) = 0;
		virtual void locate_nearest_neighbours(neighbourhood<T>& current_neighbourhood) = 0;
		virtual point<T> get_point() = 0;
		virtual size_t get_n_leaves() =0;
};

template<typename T>
class leaf : public kd_tree<T>
{
	//protected:
	public:
		kd_tree<T> *root;

		std::vector<point<T>> leaves;
		std::vector<T> classes;

	public:
		leaf(kd_tree<T> *in_root = nullptr) : root(in_root) {}
		~leaf() {};

		void insert_point(point<T> in_point, T in_class);
		void locate_nearest_neighbours(neighbourhood<T>& current_neighbourhood);
		point<T> get_point() {return leaves.back();}
		size_t get_n_leaves() {return leaves.size();}
};

template<typename T>
class branch : public kd_tree<T>
{
	//protected:
	public:
		branch<T> *root;
		kd_tree<T> *left_child, *right_child;
		point<T> splitting_point;
		size_t depth;

	public:
		const size_t max_depth;

		branch(size_t in_max_depth, point<T> in_splitting_point, branch<T> *in_root = nullptr);
		~branch() {delete left_child; delete right_child;}

		void insert_point(point<T> in_point, T in_class);
		void locate_nearest_neighbours(neighbourhood<T>& current_neighbourhood);
		point<T> get_point() {return right_child->get_point();}
		size_t get_n_leaves() {return right_child->get_n_leaves();}
};

} // namespace tree
#endif


