//------------------------------------------------
// BOXES.CPP
//------------------------------------------------

#include "boxes.hpp"

// free functions

// member functions

template<typename T>
void box_grid<T>::insert_point(point<T> in_point, T in_class)
{
	unsigned index(get_index(in_point, dim ,num_segments));

	boxes[index].push_back(in_point);
	classes[index].push_back(in_class);
}

template<typename T>
void box_grid<T>::locate_nearest_neighbours(neighbourhood<T> current_neighbourhood)
{
	unsigned target_index(get_index(current_neighbourhood.target, dim, num_segments));

	for(size_t i = 0; i < boxes[target_index].size(); ++i)
	{
		current_neighbourhood.insert_neighbour_candidate(
				boxes[target_index][i], classes[target_index][i]);
	}

	T max_dist(current_neighbourhood.distances.back());
	if(max_dist > uipow(2, dim))
	{
		for(const auto& dist : current_neighbourhood.distances)
		{
			if(dist > uipow(2,dim)) break;
			max_dist = dist;
		}
	}

	point<T> shifted_targets;
	for(size_t i = 0; i < current_neighbourhood.target.size(); ++i)
	{

	}
}

template void box_grid<double>::insert_point(point<double> in_point, double in_class);
