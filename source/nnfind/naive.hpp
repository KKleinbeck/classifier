#pragma once
//------------------------------------------------
//  NAIVE.HPP
//------------------------------------------------

#ifndef NAIVE_HPP
#define NAIVE_HPP

// Libraries

// My Libraries
#include "points.hpp"
#include "csvhandler.hpp"

// Typedefs and Structures

// free functions

template<typename T>
void populate_neighbourhoods(std::vector<neighbourhood<T>>& neighbourhoods, csvhandler& input);

#endif
