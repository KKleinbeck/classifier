#pragma once
//------------------------------------------------
//  BOXES.HPP
//------------------------------------------------

#ifndef BOXES_HPP
#define BOXES_HPP

#include <points.hpp>

// My Libraries

// free functions

template<typename T>
inline size_t get_index(const point<T>& P, size_t dim, size_t n)
{
	size_t index(0);
	for(const T& x : P) index = index*dim + static_cast<unsigned>(n*(x+1)/2);
	return index;
}

// Typedefs and Structures

template<typename T>
using box = std::vector<point<T>>;

template<typename T>
class box_grid
{
	private:
		size_t dim, num_segments;
		std::vector<box<T>> boxes;
		std::vector<std::vector<T>> classes;

		std::vector<unsigned> boxes_with_possible_neighbours(point<T> target, T dist);

	public:
		box_grid(size_t dimensions, size_t num_segments_in) :
			dim(dimensions), num_segments(num_segments_in),
			boxes(std::vector<box<T>>(uipow(num_segments, dim))),
			classes(std::vector<std::vector<T>>(uipow(num_segments, dim))) {};

		void insert_point(point<T> in_point, T in_class);
		void locate_nearest_neighbours(neighbourhood<T> current_neighbourhood);
};

#endif

