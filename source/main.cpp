#include <iostream>
#include <stdlib.h>

#include "iohandler.hpp"
#include "kdtree.hpp"
#include "csvhandler.hpp"

int main(int argc, char *argv[])
{
	srand(std::time(0));

	io::input_params cmd_input;
	io::parse_options(argc, argv, cmd_input);

	// Preprocess data: create randomized data samples for training
	csvhandler input(cmd_input.filename+".train.csv", std::fstream::in);

	std::vector<csvhandler> data_segments;
	for(size_t l = 0; l < cmd_input.l; ++l)
	{
		data_segments.push_back(
			csvhandler("data/tmp/"+std::to_string(l)+".csv", std::ios::out)
			);
	}

	segment_data(input, data_segments);
	
	// Train for optimal k
	std::vector<neighbourhood<double>> test_neighbourhoods;
	tree::branch<double> *data_tree;
	std::vector<unsigned> misclassifications(cmd_input.k_max, 0);
	std::vector<double> misclassification_rate(cmd_input.k_max, 0);

	for(size_t l = 0; l < cmd_input.l; ++l)
	{
		for(auto& segment: data_segments) segment.open(segment.file_name, std::ios::in);
		std::vector<double> line;
		while(data_segments[l].get_line(line))
		{
			//std::cout << line <<std::endl;
			test_neighbourhoods.push_back(
					neighbourhood<double>(point<double>(line.begin()+1, line.end()),
						cmd_input.k_max, line[0] ));
		}

		for(size_t m = 0; m < cmd_input.l; ++m)
		{
			if(m == l) continue;
			data_tree = new tree::branch<double>(3,
					point<double>(test_neighbourhoods[0].target.size(), 0) );

			point<double> input;
			while(data_segments[m].get_line(input) )
			{
				data_tree->insert_point(point<double>(input.begin() + 1, input.end()), input[0]);
			}

			for(auto& current_neighbourhood : test_neighbourhoods)
			{
				data_tree->locate_nearest_neighbours(current_neighbourhood);
			}
			delete data_tree;
		}

		for(const auto& current_neighbourhood : test_neighbourhoods)
		{
			determine_misclassifications(current_neighbourhood, misclassifications);

			for(size_t k = 0; k < cmd_input.k_max; ++k)
			{
				misclassification_rate[k] +=
					((double) misclassifications[k])/(test_neighbourhoods.size() * cmd_input.l);
			}
		}

		test_neighbourhoods.clear();
	}

	// classify test data
	size_t k_from_training(index_to_smallest(misclassification_rate)+1);
	csvhandler output(cmd_input.filename+".result.csv", std::fstream::out);
	input.open(cmd_input.filename+".test.csv", std::fstream::in);
	for(auto& segment: data_segments) segment.open(segment.file_name, std::ios::out);

	segment_data(input, data_segments);
	test_neighbourhoods.clear();
	misclassifications.resize(k_from_training);
	double final_misclassification_rate(0);

	for(size_t l = 0; l < cmd_input.l; ++l)
	{
		for(auto& segment: data_segments) segment.open(segment.file_name, std::ios::in);

		std::vector<double> line;
		while(data_segments[l].get_line(line))
		{
			test_neighbourhoods.push_back(
					neighbourhood<double>(point<double>(line.begin()+1, line.end()),
						k_from_training, line[0] ));
		}
		for(size_t m = 0; m < cmd_input.l; ++m)
		{
			if(m == l) continue;
			data_tree = new tree::branch<double>(3,
					point<double>(test_neighbourhoods[0].target.size(), 0) );

			point<double> input;
			while(data_segments[m].get_line(input) )
			{
				data_tree->insert_point(point<double>(input.begin() + 1, input.end()), input[0]);
			}

			for(auto& current_neighbourhood : test_neighbourhoods)
			{
				data_tree->locate_nearest_neighbours(current_neighbourhood);
			}
			delete data_tree;
		}

		std::cout << final_misclassification_rate << std::endl;
		for(const auto& current_neighbourhood : test_neighbourhoods)
		{
			output.put(current_neighbourhood.target_class);
			output.write_line(current_neighbourhood.target);
			determine_misclassifications(current_neighbourhood, misclassifications);
			final_misclassification_rate +=
				((double) misclassifications.back())/(test_neighbourhoods.size() * cmd_input.l);
		}
		test_neighbourhoods.clear();
	}

	// Finally, destroy all temporary files
	for(auto& segment : data_segments) segment.purge();

	return 0;
}


