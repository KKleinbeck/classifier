#pragma once
//------------------------------------------------
//  CSVHANDLER.HPP
//------------------------------------------------

#ifndef CSVHANDLER_HPP
#define CSVHANDLER_HPP

// Libraries
#include <string>
#include <vector>
#include <fstream>

// My Libraries

// Typedefs and Structures

// csvhandler : handles, i.e., reads and loads csv files
class csvhandler 
{
	private:
		std::fstream file_handle;

	public:
		std::string file_name;

		csvhandler(std::string file_name_in, std::ios_base::openmode mode) :
			file_handle(file_name_in.c_str(), mode), file_name(file_name_in) {};

		bool get_line(std::vector<double> &values);
		bool write_line(const std::vector<double> &values);
		bool put(double pre_value);

		void reset_pos() { file_handle.clear(); file_handle.seekg(0); file_handle.seekp(0); }
		bool purge();
		void open(std::string file_name_in, std::ios_base::openmode mode)
		{
			file_handle.close();
			file_handle.open(file_name_in.c_str(), mode);
		}
};

// free functions

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& vec)
{
	if( !vec.empty() )
	{
		std::copy(vec.begin(), vec.end() - 1 , std::ostream_iterator<T>(out, ", "));
		out << vec.back();
	}
	return out;
}

void segment_data(csvhandler& input, std::vector<csvhandler>& segments);

#endif
