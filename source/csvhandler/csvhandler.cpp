//------------------------------------------------
// CSVHANDLER.CPP
//------------------------------------------------

#include "csvhandler.hpp"

#include <sstream>
#include <stdlib.h>
#include <stdio.h>

// free functions

void segment_data(csvhandler& input, std::vector<csvhandler>& segments)
{
	std::vector<double> data;
	const size_t num_segments = segments.size();

	while(input.get_line(data))
	{
		segments[rand() % num_segments].write_line(data);
	}

	for( auto& segment : segments) segment.reset_pos();
}


// member functions

bool csvhandler::get_line(std::vector<double> &values)
{
	std::string line;
	values.clear();

	std::getline(file_handle, line);
	if(file_handle.eof()) return false;

	size_t comma_pos = line.find(',', 0);

	for(unsigned i = 0;  comma_pos != std::string::npos; ++i)
	{
		values.push_back(stod(line.substr(0, comma_pos) ) );
		line = line.substr(comma_pos + 1);
		comma_pos = line.find(',', 0);
	}
	values.push_back(stod(line));

	return true;
}

bool csvhandler::write_line(const std::vector<double> &values)
{
	std::ostringstream output;
	output << values;
	file_handle << output.str()  << std::endl;

	return file_handle.good();
}

bool csvhandler::put(double pre_value)
{
	file_handle << pre_value << ", ";

	return file_handle.good();
}

bool csvhandler::purge()
{
	if(!file_handle.is_open() ) return false;
	
	file_handle.close();
	if(!remove(file_name.c_str() ) )
	{
		return true;
	}

	return false;
}
